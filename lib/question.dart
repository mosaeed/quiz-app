import 'package:flutter/material.dart';
import 'package:quiz_app_1/main.dart';

class Question extends StatelessWidget {
  Question(this.questionText);

  final String questionText;

  @override
  Widget build(BuildContext context) {
    return Container(
        width: double.infinity,
        margin: EdgeInsets.all(10.0),
        child: Text(
          questionText,
          style: TextStyle(fontSize: 30,color: b,fontWeight: FontWeight.bold),
          textAlign: TextAlign.center,
        ));
  }
}
