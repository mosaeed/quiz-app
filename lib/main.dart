import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:quiz_app_1/answer.dart';
import 'package:quiz_app_1/question.dart';
import 'package:quiz_app_1/quiz.dart';
import 'package:quiz_app_1/result.dart';

main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

Color b = Colors.black;
Color w = Colors.white;

class _MyAppState extends State<MyApp> {
  bool isSwitched = false;

  int _questionIndex = 0;
  int _totalScore = 0;
  int _num0 = 0;
  int _num1 = 0;
  int _num2 = 0;
  final List<Map<String, Object>> _question = [
    {
      "questionText": "What's your favorite color?",
      "answers": [
        {"text": 'Black', "score": 10},
        {"text": 'Blue', "score": 20},
        {"text": 'Green', "score": 30},
        {"text": 'Yellow', "score": 40},
      ],
    },
    {
      "questionText": "What's your favorite animal?",
      "answers": [
        {"text": 'Rabbit', "score": 10},
        {"text": 'Tiger', "score": 20},
        {"text": 'Elephant', "score": 30},
        {"text": 'Lion', "score": 40},
      ],
    },
    {
      "questionText": "What's your favorite country?",
      "answers": [
        {"text": 'Egypt', "score": 10},
        {"text": 'America', "score": 20},
        {"text": 'France', "score": 30},
        {"text": 'Germany', "score": 40},
      ],
    },
  ];

  void answerQuestion(int score) {
    if (_questionIndex == 0) {
      _num0 = score;
    } else if (_questionIndex == 1) {
      _num1 = score;
    } else if (_questionIndex == 2) {
      _num2 = score;
    }

    _totalScore += score;
    setState(() {
      _questionIndex++;
    });
  }

  void resetQuiz() {
    setState(() {
      _totalScore = 0;
      _questionIndex = 0;
      _num0 = 0;
      _num1 = 0;
      _num2 = 0;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: Text(
            "Quiz App",
            style: TextStyle(color: w),
          ),
          actions: [
            Switch(
                activeColor: Colors.white,
                inactiveThumbColor: Colors.black,
                inactiveTrackColor: Colors.black,
                value: isSwitched,
                onChanged: (value) {
                  setState(() {
                    isSwitched = value;
                    if (isSwitched) {
                      b = Colors.white;
                      w = Colors.black;
                    } else {
                      b = Colors.black;
                      w = Colors.white;
                    }
                  });
                })
          ],
        ),
        body: Container(
            color: w,
            child: _questionIndex < _question.length
                ? Quiz(_question, _questionIndex, answerQuestion)
                : Result(resetQuiz, _totalScore)),
        floatingActionButton: Visibility(
          visible: (_questionIndex == 0||_questionIndex==3) ? false : true,
          child: FloatingActionButton(
            child: Icon(
              Icons.arrow_back,
              color: w,
            ),
            onPressed: () {
              setState(() {
                if (_questionIndex == 1) {
                  _questionIndex--;
                  _totalScore -= _num0;
                } else if (_questionIndex == 2) {
                  _totalScore -= _num1;
                  _questionIndex--;
                } else if (_questionIndex == 3) {
                  _totalScore -= _num2;
                  _questionIndex--;
                }
              });
            },
          ),
        ),
      ),
    );
  }
}
