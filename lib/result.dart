import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:quiz_app_1/main.dart';

class Result extends StatelessWidget {
  final Function() resetQuiz;
  final int totalScore;

  Result(this.resetQuiz, this.totalScore);

  String get resultPhrase {
    String resultText;
    if (totalScore >= 70) {
      resultText = "You are awesome!";
    } else if (totalScore >= 70) {
      resultText = "Pretty likable!";
    } else {
      resultText = "You are so bad!";
    }
    return resultText;
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            "Your score is $totalScore",
            style: TextStyle(
              fontSize: 35,
              fontWeight: FontWeight.bold,
              color: b
            ),
            textAlign: TextAlign.center,
          ),
          Text(
            resultPhrase,
            style: TextStyle(
              fontSize: 45,
              fontWeight: FontWeight.bold,
              color: b
            ),
            textAlign: TextAlign.center,
          ),
          Padding(
            padding: EdgeInsets.only(top: 8.0),
            child: TextButton(
                onPressed: resetQuiz,
                child: Text(
                  "Restart The App",
                  style: TextStyle(fontSize: 30, color: Colors.blue),
                )),
          )
        ],
      ),
    );
  }
}
